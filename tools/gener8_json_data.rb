# simple ruby script to
# - fetch the haml data files from our github repo
# - convert it into nice JSON
# - store it locally in a file named r8cities.json

require 'open-uri'
require 'json'

cities = ["bangkok", "chiang-mai", "berlin", "budapest", "kuala-lumpur", "warsaw"]

def process_city(page_string)
	cityDict = {}
	cities = []
	city = {}
	page_string.lines.each { |line|
		if line.start_with? "title:"
			cityDict["name"] = line.split(":")[1].strip
			puts("found city name: "+cityDict["name"])
		else
			line.strip!
			if line.start_with? "- "
				# new city
				kv = line[2..line.length].split(":",2)
				# create a new city
				city = { kv[0].strip => kv[1].strip }
				#add it to our array
				cities.push(city)
			else #this is really dumb but... whatever!
				kv = line.split(":",2)
				if kv[0] && kv[1]
				  city[kv[0].strip] = kv[1].strip
				end
			end
		end
		cityDict["cities"] = cities
	}
	return cityDict
end

#convert city to JSON via our github files
# note: This only works because our repo is open...
citiesDict = cities.map { |city|
	puts("reading: "+city)
	url = "https://raw.githubusercontent.com/ytspar/ristrettogram/master/source/"+city+".html.haml"
	result = {}
	open(url) do |f|
  		page_string = f.read
		#print("GOT Page: "+page_string)
		result = process_city(page_string)
	end
	result
}
prettyJson = JSON.pretty_generate(citiesDict)

puts("Json: "+prettyJson)

File.open("r8cities.json", 'w') {|f| f.write(prettyJson) }
